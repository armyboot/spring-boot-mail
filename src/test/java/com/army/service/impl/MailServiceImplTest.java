package com.army.service.impl;

import com.army.SpringBootMailApplicationTests;
import com.army.service.MailService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import static org.junit.Assert.*;

@Slf4j
@Component
public class MailServiceImplTest extends SpringBootMailApplicationTests {

    @Autowired
    private MailService mailService;

    @Autowired
    private TemplateEngine templateEngine;

    @Test
    public void sendSimpleMail() {
        String to = "army.qin@easternphoenix.com";
        String subject = "Army Home";
        String content = "大家好,Spring Boot Mail邮件.";
        mailService.sendSimpleMail(to, subject, content);
    }

    @Test
    public void sendHtmlMail() {
        String to = "army.qin@easternphoenix.com";
        String subject = "Html邮件";
        String content = "<html>\n" +
                "<body>\n"+
                "    <h3>Hi Army, 这是一封HTML邮件！</h3>\n" +
                "    <h3>这是一句换行语句！</h3>\n\n\n" +
                "    <h3>这是一句换换行语句！</h3>\n" +
                "</body>\n" +
                "</html>";
        mailService.sendHtmlMail(to, subject, content);
    }

    @Test
    public void sendAttachmentsMail() {
        String to = "190848285@qq.com";
        String subject = "带附件邮件";
        String content = "有附件，请查收!";
        String filePath = "C:/Users/vm/Pictures/1.jpg";

        mailService.sendAttachmentsMail(to, subject, content, filePath);
    }

    @Test
    public void sendInlineResourceMail() {
        String to = "army.qin@easternphoenix.com";
        String subject = "有图片附件";
        String rscId = "meini";
        String content="<html><body>这是有图片的邮件：<img src=\'cid:" + rscId + "\' ></body></html>";
        String imgPath = "C:/Users/vm/Pictures/1.jpg";

        mailService.sendInlineResourceMail(to, subject, content, imgPath, rscId);
    }

    @Test
    public void sendTemplateEmail() {
        Context context = new Context();
        context.setVariable("id", "168");
        String emailContent = templateEngine.process("emailTemplate", context);

        mailService.sendHtmlMail("army.qin@easternphoenix.com", "这是模板邮件", emailContent);
    }
}